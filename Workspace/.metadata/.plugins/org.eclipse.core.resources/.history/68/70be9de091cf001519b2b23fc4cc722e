#include "WPILib.h"
#include "TankDrive.h"
#include "Drivetrain.h"
#include "Suspension.h"
#include "Settings.h"

class Robot: public IterativeRobot
{
private:
    LiveWindow *lw = LiveWindow::GetInstance();
    SendableChooser *chooser;
    const std::string autoNameDefault = "Default";
    const std::string autoNameCustom = "My Auto";
    std::string autoSelected;

    TankDrive* pTankDrive;
    Suspension* pSuspension;

    void RobotInit()
    {
        chooser = new SendableChooser();
        chooser->AddDefault(autoNameDefault, (void*) &autoNameDefault);
        chooser->AddObject(autoNameCustom, (void*) &autoNameCustom);
        SmartDashboard::PutData("Auto Modes", chooser);
    }

    /**
     * This autonomous (along with the chooser code above) shows how to select between different autonomous modes
     * using the dashboard. The sendable chooser code works with the Java SmartDashboard. If you prefer the LabVIEW
     * Dashboard, remove all of the chooser code and uncomment the GetString line to get the auto name from the text box
     * below the Gyro
     *
     * You can add additional auto modes by adding additional comparisons to the if-else structure below with additional strings.
     * If using the SendableChooser make sure to add them to the chooser code above as well.
     */
    void AutonomousInit()
    {
        autoSelected = *((std::string*) chooser->GetSelected());
        //std::string autoSelected = SmartDashboard::GetString("Auto Selector", autoNameDefault);
        std::cout << "Auto selected: " << autoSelected << std::endl;

        if (autoSelected == autoNameCustom)
        {
            //Custom Auto goes here
        }
        else
        {
            //Default Auto goes here
        }
    }

    void AutonomousPeriodic()
    {
        if (autoSelected == autoNameCustom)
        {
            //Custom Auto goes here
        }
        else
        {
            //Default Auto goes here
        }
    }

    void TeleopInit()
    {
        Joystick* pJoystick = new Joystick(JOYSTICK_PORT_DRIVE);
        Drivetrain* pDrivetrain = new Drivetrain(CAN_ID_TALON_LF,
                CAN_ID_TALON_RF, CAN_ID_TALON_LR, CAN_ID_TALON_RR);
        pTankDrive = new TankDrive(pJoystick, pDrivetrain);

        Solenoid* pSolenoidFront = new Solenoid(CAN_ID_SUSPENSION_FRONT,
                PCM_PORT_SOLENOID_SUSPENSION_FRONT)
        Solenoid* pSolenoidRear = new Solenoid(CAN_ID_SUSPENSION_REAR,
                PCM_PORT_SOLENOID_SUSPENSION_REAR)
        pSuspension = new Suspension(pSolenoidFront, pSolenoidRear);
    }

    void TeleopPeriodic()
    {
        pTankDrive->Update();
    }

    void TestPeriodic()
    {
        lw->Run();
    }
};

START_ROBOT_CLASS(Robot)
