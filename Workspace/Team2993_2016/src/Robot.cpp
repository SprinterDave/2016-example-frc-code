#include "Robot.h"

#include <iomanip>

// RobotInit->
// DisabledInit->
// DisabledPeriodic->
// AutonomousInit->
// AutonomousPeriodic->
// DisabledInit->
// DisabledPeriodic

void Robot::RobotInit()
{
    std::cout << "=== RobotInit ==" << std::endl;

    pChooser = new SendableChooser();
    pAutonomous = new Autonomous(pChooser, this);
    pDrive = new Drive();
    pSuspension = new Suspension();
    pVoltageMonitor = new VoltageMonitor(batteryThresholdVoltage, batterySampleInterval, batteryAverageWindow);
    pCompressor = new Compressor();
    pClimbingArm = new ClimbingArm();
    pClaw = new Claw();

    pCompressor->Start();
}

void Robot::AutonomousInit()
{
    std::cout << "=== AutonomousInit ==" << std::endl;

    autoSelected = *((std::string*) pChooser->GetSelected());
    std::cout << "Auto selected: " << autoSelected << std::endl;

    pAutonomous->RunInit(autoSelected);
}

void Robot::AutonomousPeriodic()
{
    pAutonomous->RunPeriodic(autoSelected);
}

void Robot::TeleopInit()
{
    std::cout << "=== TeleopInit ==" << std::endl;
}

void Robot::TeleopPeriodic()
{
    pDrive->Update();
    pClimbingArm->Update();
    pClaw->Update();
    VoltageCheck();
}

void Robot::TestInit()
{
    std::cout << "=== TestInit ==" << std::endl;
}

void Robot::TestPeriodic()
{
    lw->Run();
}

void Robot::DisabledInit()
{
    std::cout << "=== DisabledInit ==" << std::endl;
}

void Robot::DisabledPeriodic()
{

}


void Robot::VoltageCheck()
{
    if (!pVoltageMonitor->Monitor())
    {
        float voltage = pVoltageMonitor->GetAverageVoltage();
        std::cout.precision(2);
        std::cout << "WARNING: Voltage level is " << voltage << ", turning off compressor" << std::endl;

        pCompressor->Stop();
    }
}

START_ROBOT_CLASS(Robot)
