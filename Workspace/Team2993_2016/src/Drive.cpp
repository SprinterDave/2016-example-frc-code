#include "Drive.h"

// Joystick port to use with Drive
#define DRIVE_JOYSTICK 0
// CAN ids
#define CAN_ID_TALON_LF 1
#define CAN_ID_TALON_RF 2
#define CAN_ID_TALON_LR 3
#define CAN_ID_TALON_RR 4
// Motor inversion flags
#define LF_INVERT false
#define RF_INVERT false
#define LR_INVERT false
#define RR_INVERT false

Drive::Drive()
{
    pJoystick = new Joystick(DRIVE_JOYSTICK);
    pLF = new CANTalon(CAN_ID_TALON_LF);
    pRF = new CANTalon(CAN_ID_TALON_RF);
    pLR = new CANTalon(CAN_ID_TALON_LR);
    pRR = new CANTalon(CAN_ID_TALON_RR);

    pRobotDrive = new RobotDrive(pLF, pLR, pRF, pRR);
    pRobotDrive->SetInvertedMotor(RobotDrive::kFrontLeftMotor, LF_INVERT);
    pRobotDrive->SetInvertedMotor(RobotDrive::kFrontRightMotor, RF_INVERT);
    pRobotDrive->SetInvertedMotor(RobotDrive::kRearLeftMotor, LR_INVERT);
    pRobotDrive->SetInvertedMotor(RobotDrive::kRearRightMotor, RR_INVERT);
}

Drive::~Drive()
{
}

void Drive::Update()
{
    pRobotDrive->ArcadeDrive(pJoystick);
}
