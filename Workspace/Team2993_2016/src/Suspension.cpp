#include "Suspension.h"

// CAN ids
#define CAN_ID_SOLENOID_SUSPENSION_FRONT 5
#define CAN_ID_SOLENOID_SUSPENSION_REAR 6
// PCM port numbers for solenoids
#define PCM_PORT_SOLENOID_SUSPENSION_FRONT 0
#define PCM_PORT_SOLENOID_SUSPENSION_REAR 1
// Logic value to set solenoids to raise suspension
#define RAISE_SUSPENSION true
// Logic value to set solenoids to raise suspension
#define LOWER_SUSPENSION false

Suspension::Suspension()
{
    pSolenoidFront = new Solenoid(CAN_ID_SOLENOID_SUSPENSION_FRONT, PCM_PORT_SOLENOID_SUSPENSION_FRONT);
    pSolenoidRear = new Solenoid(CAN_ID_SOLENOID_SUSPENSION_REAR, PCM_PORT_SOLENOID_SUSPENSION_REAR);
}

Suspension::~Suspension()
{
}

void Suspension::Raise()
{
    pSolenoidFront->Set(RAISE_SUSPENSION);
    pSolenoidRear->Set(RAISE_SUSPENSION);
}

void Suspension::Lower()
{
    pSolenoidFront->Set(LOWER_SUSPENSION);
    pSolenoidRear->Set(LOWER_SUSPENSION);
}

void Suspension::RaiseFront()
{
    pSolenoidFront->Set(RAISE_SUSPENSION);
}

void Suspension::RaiseRear()
{
    pSolenoidRear->Set(RAISE_SUSPENSION);
}

void Suspension::LowerFront()
{
    pSolenoidFront->Set(LOWER_SUSPENSION);
}

void Suspension::LowerRear()
{
    pSolenoidRear->Set(LOWER_SUSPENSION);
}


