#ifndef _INCLUDED_AUTONOMOUS_H_
#define _INCLUDED_AUTONOMOUS_H_

#include <WPILib.h>

#include <string>
#include <map>

class Robot;

class Autonomous
{
    public:
        Autonomous(SendableChooser* pChooser, Robot* pRobot);
        ~Autonomous();

        void RunInit(std::string name);
        void RunPeriodic(std::string name);

    private:
        Robot* pRobot;

        void MoatCrossAndLowGoal2Init();
        void MoatCrossAndLowGoal2Periodic();

        void MoatCrossAndLowGoal3Init();
        void MoatCrossAndLowGoal3Periodic();

        void MoatCrossAndLowGoal4Init();
        void MoatCrossAndLowGoal4Periodic();

        void MoatCrossAndLowGoal5Init();
        void MoatCrossAndLowGoal5Periodic();

        typedef void (Autonomous::*Func)(void);
        std::map<std::string, Func> initFuncs;
        std::map<std::string, Func> periodicFuncs;
};

#endif
