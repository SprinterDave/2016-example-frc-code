#ifndef _INCLUDED_DRIVE_H_
#define _INCLUDED_DRIVE_H_

#include "Joystick.h"
#include "CANTalon.h"
#include "RobotDrive.h"

class Drive
{
    private:
        Joystick* pJoystick;
        CANTalon* pLF;
        CANTalon* pRF;
        CANTalon* pLR;
        CANTalon* pRR;

        RobotDrive* pRobotDrive;

    public:
        Drive();
        ~Drive();

        void Update();
};

#endif
