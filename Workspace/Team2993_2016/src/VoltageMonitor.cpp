#include "VoltageMonitor.h"
#include "Timer.h"
#include "DriverStation.h"

VoltageMonitor::VoltageMonitor(float voltageThreshold, double sampleIntervalMsec, double sampleWindowMsec)
{
    this->voltageThreshold = voltageThreshold;
    this->sampleIntervalMsec =sampleIntervalMsec;
    lastSampleTimeMsec = GetTime();

    maxSamples = sampleWindowMsec / sampleIntervalMsec;
    numSamples = 0;
}

VoltageMonitor::~VoltageMonitor()
{
}

double VoltageMonitor::GetTimeMsec()
{
    return GetTime() / 1000;
}

float VoltageMonitor::GetAverageVoltage()
{
    return averageVoltage;
}

bool VoltageMonitor::Monitor()
{
    double timeMsec = GetTimeMsec();

    if ( timeMsec >= lastSampleTimeMsec + sampleIntervalMsec )
    {
        float voltage = DriverStation::GetInstance().GetBatteryVoltage();
        voltageSum += voltage;
        numSamples++;
        if (numSamples == maxSamples)
        {
            voltageSum -= priorVoltageSample;
            numSamples--;
        }
        averageVoltage = voltageSum / maxSamples;
        priorVoltageSample = voltage;
    }
    return numSamples < maxSamples || averageVoltage > voltageThreshold;
}

